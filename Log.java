package version0;

/**
 * This class is used to write a log so errors can be followed up on properly
 *
 * @author Dominik Bartl
 *
 * History:
 * ========
 * 17.10.2019 File created
 *
 */

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class Log {

	/**
	 * This method tries to open a file called "logfile.txt". If this fails, the System exits with status -1.
	 *
	 * If the file is opened, the message given by the parameter msg is written to the file, if it's not empty.
	 * Following the message, the exception given by the parameter ex is written to the file, if it's not null.
	 *
	 * @param msg - The message to be written to the log
	 * @param ex - The exception thrown whose StackTrace has to be written to the log
	 */
	
	private static void inner_log(String msg, Exception ex) {
		FileWriter fw;
		try {
			fw = new FileWriter("logfile.txt");
			BufferedWriter bw = new BufferedWriter(fw);
			if(!msg.isEmpty()) {
				bw.write(msg + "\n");
			}
			if(ex != null) {
				for(StackTraceElement stackTraceElement : ex.getStackTrace()) {
					bw.write(stackTraceElement.toString() + "\n");
				}
			}
			bw.close();
		} catch (IOException e) {
			System.exit(-1);
		}
	}

	/**
	 * This method writes a message but no exception to the log by passing it as a parameter to the method inner_log()
	 *
	 * @param msg - The message to be written to the log.
	 */
	
	public static void log(String msg) {
		Log.inner_log(msg, null);
	}


	/**
	 * This method writes a message and exception to the log by passing them as a parameter to the method inner_log()
	 *
	 * @param msg - The message to be written to the log.
	 * @param ex - The exception to be written to the log
	 */
	
	public static void log(String msg, Exception ex) {
		Log.inner_log(msg, ex);
	}
}

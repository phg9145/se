package version0;

/**
 * This is the class with the purpose to compare files with each other.
 *
 * @author Leon Kattendick
 *
 * History:
 * ========
 * 17.10.2019 File created
 */

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;

public class FileComparer {

	private File source;
	private File target;

	public FileComparer(File source, File target) {
		this.source = source;
		this.target = target;
	}

	/**
	 * This method compares the two Files "source" and "target"  with each other and returns true if they are equal.
	 * The Files are given to the constructor of this class via parameter which assigns them to private variables.
	 *
	 * First, both files are used as input for a FileInputStream. Then, the length and the first 4096 bytes of each file are saved in local variables.
	 * Eventually, both FileInputStreams are closed again and the local variables are compared.
	 *
	 * @return false if either the length of both files or the first 4096 bytes of both files are not equal
	 * @return true if both length as well as the first 4096 bytes of both files are equal.
	 * @throws IOException
	 */

	public boolean compareFiles() throws IOException {
		
		FileInputStream inSource = new FileInputStream(source);
		FileInputStream inTarget = new FileInputStream(target);
		
		byte[] contentSource = new byte[4096];
		byte[] contentTarget = new byte[4096];
		
		int lengthSource = inSource.read(contentSource);
		int lengthTarget = inTarget.read(contentTarget);
		
		inSource.close();
		inTarget.close();
		
		if(lengthSource != lengthTarget || !Arrays.equals(contentSource, contentTarget)) {
			return false;
		}
		
		return true;
	}
}

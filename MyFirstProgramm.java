package version0;
import java.io.File;
import java.io.IOException;


public class MyFirstProgramm {

    /**
     * This is the main-method of the project. Its purpose is to try to read two files and compare them to each other every 10 seconds
     * If the content of the files is different, the content of the source file is copied to the target file.
     *
     * First, it is checked if the correct number of arguments is given. If the number is not exactly two (source & target file), a line is written to the log and the method exits with status -1.
     * Afterwards, both files are created with the names given by the arguments.
     * Before anything is done with the files, it is checked if they even exist and in case they do, if they are readable/writable.
     * If they are not, a message is printed and the system exits with status -1.
     *
     * Eventually, a new FileComparer is created.
     * This comparer is now used in the while loop which checks every 10 seconds if the files are different to each other.
     * If that's the case, the contents of the source file are transfered into the target file via the transferFileContent()-method of the class FileTransfer.
     *
     *
     * @param args - The names of the files that are to be compared. The first argument is the source file, the second the target file
     * @throws IOException
     * @throws InterruptedException
     */

	public static void main( String [] args ) throws IOException, InterruptedException {

		if(args.length != 2) {
			Log.log("Es wurde nicht die korrekte Anzahl an Argumente angegeben. Es müssen zwei Dateipfade angegeben werden!");
			System.exit(-1);
		}

		// Leons Klasse

        File source = new File(args[0]);
        File target = new File(args[1]);

        if(source == null || target == null) {
            System.out.println("Dateien können nicht gefunden werden!");
            System.exit(-1);
        }
        if(!source.canRead() || !target.canRead() || !target.canWrite()) {
            System.out.println("Dateien können nicht gelesen/geschrieben werden!");
            System.exit(-1);
        }

        FileComparer comparer = new FileComparer(source, target);

        while (true) {
            if (!comparer.compareFiles()) {
                FileTransfer.transferFileContent(source, target);
            }
            Thread.sleep(10000);
        }

        /*
        // Berdls Klasse
		MyFirstPrototype prototyp = new MyFirstPrototype( new File( args[ 0 ] ),  new File( args[ 1 ] ) );

		while( true ) {

			if( ! prototyp.doIt() )

				MyHelper.helpMe( new File( args[ 0 ] ),  new File( args[ 1 ] ) );

			Thread.sleep( 10000 );
		}*/
	}
}

package version0;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * This is the class with the purpose to transfer file contents
 *
 * @author Leon Kattendick
 *
 * History:
 * ========
 * 17.10.2019 File created
 */

public class FileTransfer {

	/**
	 * This method creates a FileInputStream 'fIn' from the source file as well as a FileOutputStream 'fOut' from the TARGET FILE:
	 * It then transfers the contents from 'fIn' to 'fOut' with the help of the local byte-array named 'content'.
	 *
	 * Eventually, it closes both streams and the method ends.
	 *
	 * @param source - The source file
	 * @param target - The target file
	 * @throws IOException
	 */

	public static void transferFileContent(File source, File target) throws IOException {

		FileInputStream fIn = new FileInputStream(source);
		FileOutputStream fOut = new FileOutputStream(target);

		byte[] content = new byte[4096];

		int length = fIn.read(content);
		fOut.write(content, 0, length);

		fIn.close();
		fOut.close();
		
	}
}

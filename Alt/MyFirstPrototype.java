package version0;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class MyFirstPrototype {

	private File i;
	private File y;

	public MyFirstPrototype(File filea, File fileb) {
		this.i = filea;
		this.y = fileb;
	}

	public boolean doIt() throws IOException {
		try {
			final FileReader fileReaderFirst = new FileReader(i);
			final FileReader fileReaderSecond = new FileReader(y);
			
			BufferedReader bufReaderFirst = new BufferedReader(fileReaderFirst);
			BufferedReader bufReaderSecond = new BufferedReader(fileReaderSecond);
			String strFirst = null;
			String strSecond = null;

			boolean run = true;
			while(run) {
				strFirst  = bufReaderFirst .readLine();
				strSecond = bufReaderSecond.readLine(); 
				run = (strFirst != null && strSecond != null && strFirst.compareTo(strSecond) == 0);
			}
			bufReaderFirst.close();
			bufReaderSecond.close();
			
			return strFirst == null && strSecond == null;
		} catch(FileNotFoundException fnfe) {
			Log.log("Fehler beim �ffnen der Dateien.", fnfe);
			System.exit(-1);
		}
		
		return false;
	}
}